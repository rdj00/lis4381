<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Project 1">
		<meta name="author" content="R.J. Johns">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong>     - Create My Business Card App
    - Provide screenshots of complete app
    - Complete Skillsets 7 - 9 and upload screenshots
				</p>

				<h4>Android Studio Installation</h4>
				<img src="img/MyBusinessCard1.png" class="img-responsive center-block" alt="Android Studio Installation">
				<img src="img/MyBusinessCard2.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>Skillset 7</h4>
				<img src="img/ss7.png" class="img-responsive center-block" alt="Skillset 7">
				
				<h4>Skillset 8</h4>
				<img src="img/ss8.png" class="img-responsive center-block" alt="Skillset 8">
				
				<h4>Skillset 9</h4>
				<img src="img/ss9.png" class="img-responsive center-block" alt="Skillset 9">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
 