> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Ryan Johns

### Project 1 Requirements:

*Development:*

1. Create a Business Card App using Android Studio.
2. Complete Skillsets 7-9

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s first user interface; 
* Screenshot of running application’s second user interface;  


#### Assignment Screenshots:



| Screen 1    | Screen 2    |
| ----------- | ----------- |
| ![Android Studio Emulator Screen 1](img/MyBusinessCard1.png "Healthy Recipes Screen 1")      | ![Android Studio Emulator Screen 2](img/MyBusinessCard2.png "Healthy Recipes Screen 2")        |

| Skillset 7    | Skillset 8    | Skillset 9    | 
| ---------- | ----------- | ----------- |
| ![Skillset 7 - Random Number Generation w/ Validation](img/ss7.png "Screenshot of Random Number Generation w/Validation")            | ![Skillset 8 - Largest Three Numbers](img/ss8.png "Largest Three Numbers")             | ![Skillset 9 - Arrays & Runtime Validation](img/ss9.png "Screenshot of Arrays and Run Time Validation")               |




