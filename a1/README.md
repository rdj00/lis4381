> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Ryan Johns

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and BitBucket
2. Development Installations
3. Chapter Questions (Chs 1,2)
    

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [](myphpinstallation/php_installation.)
* Screenshot of Java Hello World
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions
* BitBucket Repo links
    - This assignment
    - Completed tutorials above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - This command creates an empty Git repository - basically a .git directory with            subdirectories for objects, refs/heads, refs/tags, and template files. An initial branch without any commits will be created (see the --initial-branch option below for its name).
2. git status - Displays paths that have differences between the index file and the current HEAD commit, paths that have differences between the working tree and the index file, and paths in the working tree that are not tracked by Git (and are not ignored by gitignore[5]).
3. git add - This command updates the index using the current content found in the working tree, to prepare the content staged for the next commit.
4. git commit - Create a new commit containing the current contents of the index and the given log message describing the changes.
5. git push - Updates remote refs using local refs, while sending objects necessary to complete the given refs.
6. git pull - Incorporates changes from a remote repository into the current branch. 
7. git show - Shows one or more objects (blobs, trees, tags and commits).

#### Assignment Screenshots:

> Screenshot of AAMPS running MY PHP Installation 

![Screenshot of running java MY PHP Installation](img/php.png "AAMPS Screenshot")

> Screenshot of running java Hello on VS Code

![Screenshot of running java Hello on VS Code](img/java_hello.png "A1 VS Code Screenshot")

> Screenshot of Android Studio - My First App

![Screenshot of Android Studio - My First App](img/my_first_app.png "A1 Android Studio Screenshot")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
