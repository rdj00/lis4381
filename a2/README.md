> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Ryan Johns

### Assignment 2 Requirements:

*Development:*

1. Create a mobile recipe app using Android Studio.

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s first user interface; 
* Screenshot of running application’s second user interface;  


#### Assignment Screenshots:



| Screen 1    | Screen 2    |
| ----------- | ----------- |
| ![Android Studio Emulator Screen 1](img/a2_healthy_recipes1.png "Healthy Recipes Screen 1")      | ![Android Studio Emulator Screen 2](img/a2_healthy_recipes2.png "Healthy Recipes Screen 2")        |

| Skillset 1    | Skillset 2    | Skillset 3    | 
| ----------- | ----------- | ----------- |
| ![Skillset 1 - Even Or Odd](img/ss1_EvenOrOdd.png "Screenshot of Even or Odd")            | ![Skillset 2 - Largest Number](img/ss2_LargestNumber.png "Screenshot Largest Number")             | ![Skillset 3 - Arrays & Loops](img/ss3_ArraysAndLoops.png "Screenshot of Arrays and Loops")               |




