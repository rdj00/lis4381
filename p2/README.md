> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Ryan Johns

### Project 2 Requirements:

*Development:*

1. Create a mobile recipe app using Android Studio.

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s first user interface; 
* Screenshot of running application’s second user interface;  


#### Assignment Screenshots:



| Screen 1    | Screen 2    |
| ----------- | ----------- |
| ![index.php](img/p2_1.png "Index.php")      | ![edit_petstore.php](img/p2_2.png "edit_petstore.php")        |

| Screen 3    | FAILED VALIDATION    |
| ----------- | ----------- |
| ![index.php](img/p2_3.png "Index.php")      | ![edit_petstore.php](img/p2_4.png "edit_petstore.php")        |


| Screen 5    | DELETE    |
| ----------- | ----------- |
| ![index.php](img/p2_5.png "Index.php")      | ![edit_petstore.php](img/p2_6.png "edit_petstore.php")        |


| DELETED    | RSS    |
| ----------- | ----------- |
| ![index.php](img/p2_7.png "Index.php")      | ![edit_petstore.php](img/p2_8.png "edit_petstore.php")        |





