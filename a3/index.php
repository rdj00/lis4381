<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong>    - Create ERD based upon business rules 
    - Provide screenshots of completed ERD
    - Provide DB resource links
    - Complete Skillsets 4 - 6 and upload screenshots
				</p>

				<h4>MySQL ERD</h4>
				<img src="img/a3erd.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/a3.screen1.png" class="img-responsive center-block" alt="Android Studio Installation">
				<img src="img/a2.screen2.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>Skillset 4</h4>
				<img src="img/ss4.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<h4>Skillset 5</h4>
				<img src="img/ss5.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<h4>Skillset 6</h4>
				<img src="img/ss6.png" class="img-responsive center-block" alt="AMPPS Installation">


				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
 