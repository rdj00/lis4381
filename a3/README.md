> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Ryan Johns

### Assignment 3 Requirements:

*Development:*

1. Create a database.
2. Create a Mobile APP

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of ERD; 
* Screenshot of running application’s **opening** user; interface;
* Screenshot of running application’s **processing** user; input;    
* Screenshot of running application’s **each** table;
    1. Link to a3.mwb
    [a3.mwb](https://bitbucket.org/rdj00/lis4381/src/master/a3/docs/a3.mwb)
    2. a3.sql
    [a3.sql](https://bitbucket.org/rdj00/lis4381/src/master/a3/docs/a3.mwb) 



#### Assignment Screenshots:



| Screen 1    | Screen 2    |
| ----------- | ----------- |
| ![Android Studio Emulator Screen 1](img/a3.screen1.png "My Event Screen 1")      | ![Android Studio Emulator Screen 2](img/a3.screen2.png "My Event 2")        |

| Skillset 4    | Skillset 5    | Skillset 6    | 
| ----------- | ----------- | ----------- |
| ![Skillset 4 - Even Or Odd](img/ss4.png "Screenshot of Even or Odd")            | ![Skillset 5 - Largest Number](img/ss5.png "Screenshot Largest Number")             | ![Skillset 6 - Arrays & Loops](img/ss6.png "Screenshot of Arrays and Loops")               |

![A3 ERD](img/a3erd.png "MySQL") 

| Table 1    | Table 2    | Table 3    | 
| ----------- | ----------- | ----------- |
| ![Skillset 1 - Even Or Odd](img/a3table1.png "Screenshot of Even or Odd")            | ![Skillset 2 - Largest Number](img/a3table2.png "Screenshot Largest Number")             | ![Skillset 3 - Arrays & Loops](img/a3table3.png "Screenshot of Arrays and Loops")               |




