> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Ryan Johns

### LIS4381 Requirements:

*Course Work Links:*

1. [A1_README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create BitBucket Repo
    - Complete BitBucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command description

2. [A2_README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of complete app
    - Complete Skillsets 1 - 3 and upload screenshots

3. [A3_README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules 
    - Provide screenshots of completed ERD
    - Provide DB resource links
    - Complete Skillsets 4 - 6 and upload screenshots

4. [A4_README.md](a4/README.md "My A4 README.md file")
    - Provide screenshots of Online Portfolio
    - Add JQuery validation and regular expressions to A4 index.php
    - Complete Skillsets 10 - 12 and upload screenshots

5. [A5_README.md](a5/README.md "My A5 README.md file")
    - Screenshots of Petstore Directory
    - Add Validation and placeholders
    - Skillsets 13 - 15

6. [P1_README.md](p1/README.md "My P1 README.md file")
    - Create My Business Card App
    - Provide screenshots of complete app
    - Complete Skillsets 7 - 9 and upload screenshots

7. [P2_README.md](p2/README.md "My P2 README.md file")
    - Screenshots of Petstore Directory
    - Add EDIT and DELETE Button
    - Show RSS Feed


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
