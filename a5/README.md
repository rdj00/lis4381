> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Ryan Johns

### Assignment 5 Requirements:

*Development:*

1. Create online Portfolio.
2. Create Pet Store
3.  [localhost](http://localhost:8080/lis4381/a5/add_petstore.php)

#### README.md file should include the following items:

- Provide screenshots of Online Petstore
- Add JQuery validation and regular expressions to A5 index.php
- Complete Skillsets 13 - 15 and upload screenshots  


#### Assignment Screenshots:



| Screen 1    | Screen 2    | Screen 3
| ----------- | ----------- | ----------- |
| ![Portfolio 1](img/a5_1.png "Portfolio")      | ![Portfolio 2](img/a5_2.png "Portfolio")        | ![Portfolio 3](img/a5_3.png "Portfolio")        |

![Portfolio 4](img/a5_4.png)

| Skillset 13    | Skillset 14    | Skillset 15    | 
| ----------- | ----------- | ----------- |
| ![Skillset 13 - ](img/ss13.png "Screenshot of Even or Odd")            | ![Skillset 14 ](img/ss14.png "Screenshot Largest Number")             | ![Skillset 15 - ](img/ss15.png "Screenshot of Arrays and Loops")               |




