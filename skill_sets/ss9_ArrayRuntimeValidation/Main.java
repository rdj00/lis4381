class Main
{
    
    public static void main(String args[])
    {
        Methods.getRequirements();

        int arraySize;
        
        arraySize = Methods.validateArraySize();

        Methods.calculateNumbers(arraySize);
    }
}
