import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: R.J. Johns");
        System.out.println("Program checks if which of the two integers are larger.\n");
        System.out.println("Note: This program does *not* check for non-numeric characters.\n");
    }

    public static void largestNumber()
    {
            int num1, num2;
            Scanner sc = new Scanner(System.in);

            System.out.print("Enter first integer: ");
            num1 = sc.nextInt();

            System.out.print("Enter second integer: ");
            num2 = sc.nextInt();

            System.out.println();

            evaluateNumber(num1, num2);

            sc.close();
    }

    public static int getNum()
    {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    public static void evaluateNumber(int num1, int num2)
    {
        System.out.println();
        if (num1 > num2)
            System.out.println(num1 + " is larger than " + num2);
        else if (num2 > num1)
            System.out.println(num2 + " is larger than " + num1);
        else
            System.out.println("Integers are equal.");
    }
}