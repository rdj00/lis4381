import java.util.ArrayList;
import java.util.Scanner;

public class Methods
{
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements()
    {
        System.out.println("Developer: R.J. Johns");

        System.out.println("1. Program populates ArrayList of strings with user-entered animal type values.");
        System.out.println("2. Examples: Polar Bear, Guinea Pig, Dog, Cat, Bird.");
        System.out.println("3. Program continues to collect user-entered values until user types \"n\".");
        System.out.println("4. Program displays ArrayList values after each iteration, as well as size.");


        System.out.println();
    }

    public static void createArrayList()
    {

        Scanner sc = new Scanner(System.in);
        ArrayList<String> obj = new ArrayList<String>();
        String myStr = "";
        String choice = "y";
        int num = 0;

        while (choice.equals("y"))
        {
            System.out.print("Enter animal type: ");
            myStr = sc.nextLine();
            obj.add(myStr);
            num = obj.size();
            System.out.println("ArrayList elements:" + obj + "\nArrayList Size = " + num);
            System.out.print("\nContinue? Enter y or n: ");
            choice = sc.next().toLowerCase();
            sc.nextLine();
        }
    }
      
    
}

