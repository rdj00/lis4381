import java.util.Scanner;

public class Methods
{

    public static void getRequirements()
    {
        System.out.println("Developer: R.J. Johns");
        System.out.println("Program evaluates integers as even or odd.\n");
        System.out.println("Note: This program does *not* check for non-numeric characters.\n");
    }

    public static void evaluateNumber()
    {
        int num1 = 0;
        System.out.print("Enter integer: ");
        Scanner sc =  new Scanner(System.in);
        num1 = sc.nextInt();

        if (num1 % 2 == 0)
            System.out.println(num1 + " is an even number.");

        else
            System.out.println(num1 + " is an odd number");

        sc.close();

    }
}