import java.util.Scanner;

public class Methods
{
    //display operational
    public static void getRequirements()
    {
        System.out.println("Developer: R.J. Johns");
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements");
        System.out.println("1. getRequirements(): Void method displays program requirements");
        System.out.println("2. getUserInput: Void method prompts users for user input, then calls for two methods: myVoidMethod() and myValueReturningMethod().");
        System.out.println("3. myVoidMethod():");
        System.out.println("\t\t a. Accepts two arguments: String and int.");
        System.out.println("\t\t b. Prints user's first name and age.");
        System.out.println("4. myValueReturningMethod():");
        System.out.println("\t\t a. Accepts two arguments: String and int.");
        System.out.println("\t\t b. Returns String containing first name and age.");

        System.out.println();
    }

    public static void getUserInput()
    {
        // initialize variables, create Scanner object, capture user input
        String firstName="";
        int userAge = 0;
        String myStr="";
        Scanner sc = new Scanner(System.in);
        //input
        System.out.println("Enter first name: ");
        firstName=sc.next();
        System.out.println("Enter age: ");
        userAge = sc.nextInt();
        System.out.println(); // print blank line

        System.out.println("void method call: ");
        myVoidMethod (firstName, userAge);
       

        System.out.println("value-returning method call: ");
        myStr = myValueReturningMethod (firstName, userAge);
        

        System.out.println(myStr);
    }
      
    public static void myVoidMethod(String first, int age)
    {
        System.out.println(first + " is " + age);
        return;
    }

    public static String myValueReturningMethod(String first, int age)
    {
        return first + " is " + age;
    }
}

