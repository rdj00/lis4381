> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Ryan Johns

### Assignment 4 Requirements:

*Development:*

1. Create online Portfolio.
2. Create Pet Store

#### README.md file should include the following items:

- Provide screenshots of Online Portfolio
- Add JQuery validation and regular expressions to A4 index.php
- Complete Skillsets 10 - 12 and upload screenshots  


#### Assignment Screenshots:



| Screen 1    | Screen 2    | Screen 3
| ----------- | ----------- | ----------- |
| ![Portfolio 1](img/a4_1.png "Portfolio")      | ![Portfolio 2](img/a4.png "Portfolio")        | ![Portfolio 3](img/a4_2.png "Portfolio")        |

| Skillset 10    | Skillset 11    | Skillset 12    | 
| ----------- | ----------- | ----------- |
| ![Skillset 10 - Even Or Odd](img/ss10.png "Screenshot of Even or Odd")            | ![Skillset 11 - Largest Number](img/ss11.png "Screenshot Largest Number")             | ![Skillset 12 - Arrays & Loops](img/ss12.png "Screenshot of Arrays and Loops")               |




